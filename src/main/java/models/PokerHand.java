package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PokerHand {

    private List<PlayingCard> cards = new ArrayList<>();

    public PokerHand(PlayingCard c1, PlayingCard c2, PlayingCard c3, PlayingCard c4, PlayingCard c5) {

        List<PlayingCard> cardsInput = Arrays.asList(c1, c2, c3, c4, c5);

        boolean nullCards = cardsInput.stream().anyMatch(pl->pl==null);

        if(nullCards) throw new IllegalArgumentException("Een pokerhand bestaat uit 5 bestaande speelkaarten");

        cards.addAll(cardsInput);
    }

    public List<PlayingCard> getCards() {
        return cards;
    }
}
