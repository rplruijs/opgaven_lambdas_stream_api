package models;

import java.util.Random;

public class PlayingCard {
    

    private CARDCOLOR color;
    private CARDVALUE value;

    public PlayingCard(CARDCOLOR color, CARDVALUE value) {
        this.color = color;
        this.value = value;
    }

    public static PlayingCard random(){

        int nrOfCardValues = CARDVALUE.values().length;
        int nrOfColors = CARDCOLOR.values().length;

        final Random random = new Random();

        CARDVALUE randomValue = CARDVALUE.values()[random.nextInt(nrOfCardValues)];
        CARDCOLOR randomCARDCOLOR = CARDCOLOR.values()[random.nextInt(nrOfColors)];

        return new PlayingCard(randomCARDCOLOR, randomValue);
    }

    public int getCardValue(){
        return value.getCardValue();
    }

    public String getCardColor(){
        return color.toString();
    }

    @Override
    public String toString() {
        return color + "_" + value;
    }
}



