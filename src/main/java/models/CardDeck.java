package models;

import java.util.*;

public class CardDeck {

    final static int NR_OF_CARDS = CARDCOLOR.values().length * CARDVALUE.values().length;

    public static List<PlayingCard> shuffled(){

        List<PlayingCard> deck =  new ArrayList<PlayingCard>();
        Collections.shuffle(deck);
        return deck;
    }

    public static List<PlayingCard> unshuffled(){

        List<PlayingCard> deck =  new ArrayList<PlayingCard>();

        for (CARDVALUE cardvalue : CARDVALUE.values()) {
            for (CARDCOLOR CARDCOLOR : CARDCOLOR.values()) {
                   deck.add(new PlayingCard(CARDCOLOR, cardvalue));
            }
        }
        return deck;
    }


    public static List<PlayingCard> handOf(PlayingCard... cards){
        return Arrays.asList(cards);
    }
}
