package models;

enum CARDCOLOR {
    HEARTS, SPADES, DIAMONDS, CLUBS;
}

enum CARDVALUE{

    ACE ("Aces", 14),
    KING("Kings", 13),
    QUEEN("Queens", 12),
    JACK("Jacks", 11),
    TEN("Tens", 10),
    NINE("Nines", 9),
    EIGHT("Eights", 8),
    SEVEN("Sevens", 7),
    SIX("Sixes", 6),
    FIVE("Fives", 5),
    FOUR("Fours", 4),
    THREE("Threes", 3),
    TWO("Twoos", 2);

    private String plural;
    private int cardValue;

    CARDVALUE(String plural, int cardValue) {
        this.plural = plural;
        this.cardValue =  cardValue;
    }

    public int getCardValue(){
        return cardValue;
    }


}

public class PlayingCards {

    final static PlayingCard HEARTS_ACE = new PlayingCard(CARDCOLOR.HEARTS, CARDVALUE.ACE);
    final static PlayingCard HEARTS_KING = new PlayingCard(CARDCOLOR.HEARTS, CARDVALUE.KING);
    final static PlayingCard HEARTS_QUEEN = new PlayingCard(CARDCOLOR.HEARTS, CARDVALUE.QUEEN);
    final static PlayingCard HEARTS_JACK = new PlayingCard(CARDCOLOR.HEARTS, CARDVALUE.JACK);
    final static PlayingCard HEARTS_TEN = new PlayingCard(CARDCOLOR.HEARTS, CARDVALUE.TEN);
    final static PlayingCard HEARTS_NINE = new PlayingCard(CARDCOLOR.HEARTS, CARDVALUE.NINE);
    final static PlayingCard HEARTS_EIGHT = new PlayingCard(CARDCOLOR.HEARTS, CARDVALUE.EIGHT);
    final static PlayingCard HEARTS_SEVEN = new PlayingCard(CARDCOLOR.HEARTS, CARDVALUE.SEVEN);
    final static PlayingCard HEARTS_SIX = new PlayingCard(CARDCOLOR.HEARTS, CARDVALUE.SIX);
    final static PlayingCard HEARTS_FIVE = new PlayingCard(CARDCOLOR.HEARTS, CARDVALUE.FIVE);
    final static PlayingCard HEARTS_FOUR = new PlayingCard(CARDCOLOR.HEARTS, CARDVALUE.FOUR);
    final static PlayingCard HEARTS_THREE = new PlayingCard(CARDCOLOR.HEARTS, CARDVALUE.THREE);
    final static PlayingCard HEARTS_TWO = new PlayingCard(CARDCOLOR.HEARTS, CARDVALUE.TWO);


    final static PlayingCard DIAMONDS_ACE = new PlayingCard(CARDCOLOR.DIAMONDS, CARDVALUE.ACE);
    final static PlayingCard DIAMONDS_KING = new PlayingCard(CARDCOLOR.DIAMONDS, CARDVALUE.KING);
    final static PlayingCard DIAMONDS_QUEEN = new PlayingCard(CARDCOLOR.DIAMONDS, CARDVALUE.QUEEN);
    final static PlayingCard DIAMONDS_JACK = new PlayingCard(CARDCOLOR.DIAMONDS, CARDVALUE.JACK);
    final static PlayingCard DIAMONDS_TEN = new PlayingCard(CARDCOLOR.DIAMONDS, CARDVALUE.TEN);
    final static PlayingCard DIAMONDS_NINE = new PlayingCard(CARDCOLOR.DIAMONDS, CARDVALUE.NINE);
    final static PlayingCard DIAMONDS_EIGHT = new PlayingCard(CARDCOLOR.DIAMONDS, CARDVALUE.EIGHT);
    final static PlayingCard DIAMONDS_SEVEN = new PlayingCard(CARDCOLOR.DIAMONDS, CARDVALUE.SEVEN);
    final static PlayingCard DIAMONDS_SIX = new PlayingCard(CARDCOLOR.DIAMONDS, CARDVALUE.SIX);
    final static PlayingCard DIAMONDS_FIVE = new PlayingCard(CARDCOLOR.DIAMONDS, CARDVALUE.FIVE);
    final static PlayingCard DIAMONDS_FOUR = new PlayingCard(CARDCOLOR.DIAMONDS, CARDVALUE.FOUR);
    final static PlayingCard DIAMONDS_THREE = new PlayingCard(CARDCOLOR.DIAMONDS, CARDVALUE.THREE);
    final static PlayingCard DIAMONDS_TWO = new PlayingCard(CARDCOLOR.DIAMONDS, CARDVALUE.TWO);


    final static PlayingCard CLUBS_ACE = new PlayingCard(CARDCOLOR.CLUBS, CARDVALUE.ACE);
    final static PlayingCard CLUBS_KING = new PlayingCard(CARDCOLOR.CLUBS, CARDVALUE.KING);
    final static PlayingCard CLUBS_QUEEN = new PlayingCard(CARDCOLOR.CLUBS, CARDVALUE.QUEEN);
    final static PlayingCard CLUBS_JACK = new PlayingCard(CARDCOLOR.CLUBS, CARDVALUE.JACK);
    final static PlayingCard CLUBS_TEN = new PlayingCard(CARDCOLOR.CLUBS, CARDVALUE.TEN);
    final static PlayingCard CLUBS_NINE = new PlayingCard(CARDCOLOR.CLUBS, CARDVALUE.NINE);
    final static PlayingCard CLUBS_EIGHT = new PlayingCard(CARDCOLOR.CLUBS, CARDVALUE.EIGHT);
    final static PlayingCard CLUBS_SEVEN = new PlayingCard(CARDCOLOR.CLUBS, CARDVALUE.SEVEN);
    final static PlayingCard CLUBS_SIX = new PlayingCard(CARDCOLOR.CLUBS, CARDVALUE.SIX);
    final static PlayingCard CLUBS_FIVE = new PlayingCard(CARDCOLOR.CLUBS, CARDVALUE.FIVE);
    final static PlayingCard CLUBS_FOUR = new PlayingCard(CARDCOLOR.CLUBS, CARDVALUE.FOUR);
    final static PlayingCard CLUBS_THREE = new PlayingCard(CARDCOLOR.CLUBS, CARDVALUE.THREE);
    final static PlayingCard CLUBS_TWO = new PlayingCard(CARDCOLOR.CLUBS, CARDVALUE.TWO);

    final static PlayingCard SPADES_ACE = new PlayingCard(CARDCOLOR.SPADES, CARDVALUE.ACE);
    final static PlayingCard SPADES_KING = new PlayingCard(CARDCOLOR.SPADES, CARDVALUE.KING);
    final static PlayingCard SPADES_QUEEN = new PlayingCard(CARDCOLOR.SPADES, CARDVALUE.QUEEN);
    final static PlayingCard SPADES_JACK = new PlayingCard(CARDCOLOR.SPADES, CARDVALUE.JACK);
    final static PlayingCard SPADES_TEN = new PlayingCard(CARDCOLOR.SPADES, CARDVALUE.TEN);
    final static PlayingCard SPADES_NINE = new PlayingCard(CARDCOLOR.SPADES, CARDVALUE.NINE);
    final static PlayingCard SPADES_EIGHT = new PlayingCard(CARDCOLOR.SPADES, CARDVALUE.EIGHT);
    final static PlayingCard SPADES_SEVEN = new PlayingCard(CARDCOLOR.SPADES, CARDVALUE.SEVEN);
    final static PlayingCard SPADES_SIX = new PlayingCard(CARDCOLOR.SPADES, CARDVALUE.SIX);
    final static PlayingCard SPADES_FIVE = new PlayingCard(CARDCOLOR.SPADES, CARDVALUE.FIVE);
    final static PlayingCard SPADES_FOUR = new PlayingCard(CARDCOLOR.SPADES, CARDVALUE.FOUR);
    final static PlayingCard SPADES_THREE = new PlayingCard(CARDCOLOR.SPADES, CARDVALUE.THREE);
    final static PlayingCard SPADES_TWO = new PlayingCard(CARDCOLOR.SPADES, CARDVALUE.TWO);


}
