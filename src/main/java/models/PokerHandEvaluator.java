package models;


import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class PokerHandEvaluator {


    private static Map<Integer, List<PlayingCard>> groupedByValueStream(PokerHand hand){
        return hand.getCards()
                .stream()
                .collect(Collectors.groupingBy(PlayingCard::getCardValue));
    }

    private static boolean isXOfAKind(PokerHand hand, int x){

        Map<Integer, List<PlayingCard>> cardsGrouped  = groupedByValueStream(hand);


        int maxNumberOfSameCards = cardsGrouped.values()
                .stream()
                .max(Comparator.comparingInt(List::size))
                .get().size();

        return maxNumberOfSameCards == x;

    }

    public static boolean isTwoOfAKind(PokerHand hand){
        return isXOfAKind(hand, 2);
    }

    public static boolean isThreeOfAKind(PokerHand hand){
        return isXOfAKind(hand, 3) & !isFullHouse(hand);
    }

    public static boolean isStraight(PokerHand hand){

        Optional<PlayingCard> maxCard = hand.getCards()
                .stream()
                .max(Comparator.comparingInt(PlayingCard::getCardValue));

        Optional<PlayingCard> minCard = hand.getCards()
                .stream()
                .min(Comparator.comparingInt(PlayingCard::getCardValue));


        int spread = maxCard.get().getCardValue() - minCard.get().getCardValue();


        boolean isStraightPartSpread = spread == hand.getCards().size() - 1 ;
        boolean isStraightPartAllDiff = isXOfAKind(hand, 1);

        return isStraightPartSpread && isStraightPartAllDiff && !isStraightFlush(hand) ;

    }

    public static boolean isFlush(PokerHand hand){




        final int NR_OF_CARDS = hand.getCards().size();

        boolean allSameColor =  hand.getCards()
                .stream()
                .collect(Collectors.groupingBy(PlayingCard::getCardColor))
                .size() == 1;


        return allSameColor && !isStraightFlush(hand);


        //OF


//       String colorFirstCard = hand.getCards().get(0).getCardColor();
//
//       boolean isFlush = hand.getCards()
//                              .stream()
//                              .allMatch(playingCard -> playingCard.getCardColor().equals(colorFirstCard));
//
//
//
//
//
//
//       return isFlush;


    }

    public static boolean isFullHouse(PokerHand hand){
        List<List<PlayingCard>> groups = groupedByValueStream(hand)
                .values()
                .stream()
                .sorted(Comparator.comparing(List::size))
                .collect(Collectors.toList());



        return groups.size() == 2 &&
                groups.get(0).size() == 2 &&
                groups.get(1).size() == 3;
    }

    public static boolean isFourOfAKind(PokerHand hand){
        return isXOfAKind(hand, 4);
    }

    public static boolean isStraightFlush(PokerHand hand){
        return isFlush(hand) && isStraight(hand);
    }



}
