package models;

import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class CardDeckTest {

    @Test
    public void getShuffledDeck() throws Exception {

        Map<String, PlayingCard> randomCards =  new HashMap<String, PlayingCard>();

        List<PlayingCard> deck =  CardDeck.unshuffled();
        deck.forEach(c -> randomCards.put(c.toString(), c));

        final int actualCardsInDeck = randomCards.size();
        final int EXPECTED_CARDS_IN_DECK = CardDeck.NR_OF_CARDS;


        assertEquals(EXPECTED_CARDS_IN_DECK, actualCardsInDeck);

    }


}