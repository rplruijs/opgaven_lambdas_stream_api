package models;

import org.junit.Test;

import java.util.List;

import static models.PlayingCards.*;
import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class PokerHandEvaluatorTest {


    /**
     * TWO OF A KIND TESTS
     */

    @Test
    public void isTwoOfAKind_ShouldSucceed_when_Cards_HEARTS_QUEEN_DIAMIOND_QUEEN_CLUBS_TEN_HEART_NINE_DIAMOND_TWO_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(HEARTS_QUEEN, DIAMONDS_QUEEN, CLUBS_TEN, HEARTS_NINE, DIAMONDS_TWO);
        assertTrue(PokerHandEvaluator.isTwoOfAKind(hand));
    }

    @Test
    public void isTwoOfAKind_ShouldFail_when_Cards_HEARTS_QUEEN_DIAMIOND_THREE_CLUBS_TEN_HEART_NINE_DIAMOND_TWO_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(HEARTS_QUEEN, DIAMONDS_THREE, CLUBS_TEN, HEARTS_NINE, DIAMONDS_TWO);
        assertFalse(PokerHandEvaluator.isTwoOfAKind(hand));
    }


    @Test
    public void isTwoOfAKind_ShouldFail_when_Cards_HEARTS_QUEEN_DIAMIOND_QUEEN_CLUBS_QUEEN_HEART_NINE_DIAMOND_TWO_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(HEARTS_QUEEN, DIAMONDS_QUEEN, CLUBS_QUEEN, HEARTS_NINE, DIAMONDS_TWO);
        assertFalse(PokerHandEvaluator.isTwoOfAKind(hand));
    }


    /**
     * THREE OF A KIND TESTS
     */

    @Test
    public void isThreeOfAKind_ShouldSucceed_when_Cards_HEARTS_QUEEN_DIAMIOND_QUEEN_CLUBS_QUEEN_HEART_NINE_DIAMOND_TWO_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(HEARTS_QUEEN, DIAMONDS_QUEEN, CLUBS_QUEEN, HEARTS_NINE, DIAMONDS_TWO);
        assertTrue(PokerHandEvaluator.isThreeOfAKind(hand));
    }

    @Test
    public void isThreeOfAKind_ShouldFail_when_Cards_HEARTS_QUEEN_DIAMIOND_QUEEN_CLUBS_TEN_HEART_NINE_DIAMOND_TWO_PassedIn() throws Exception {

        PokerHand hand = new PokerHand(HEARTS_QUEEN, DIAMONDS_QUEEN, CLUBS_TEN, HEARTS_NINE, DIAMONDS_TWO);
        assertFalse(PokerHandEvaluator.isThreeOfAKind(hand));
    }

    @Test
    public void isThreeOfAKind_ShouldFail_when_Cards_HEARTS_QUEEN_DIAMIOND_QUEEN_CLUBS_QUEEN_HEART_NINE_DIAMOND_TWO_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(HEARTS_QUEEN, DIAMONDS_QUEEN, CLUBS_QUEEN, SPADES_QUEEN, DIAMONDS_TWO);
        assertFalse(PokerHandEvaluator.isThreeOfAKind(hand));
    }

    @Test
    public void isThreeOfAKind_ShouldFail_when_Cards_HEARTS_QUEEN_HEARTS_TEN_DIAMONDS_TEN_DIAMONDS_QUEEN_CLUBS_QUEEN_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(HEARTS_QUEEN, HEARTS_TEN, DIAMONDS_TEN, DIAMONDS_QUEEN, CLUBS_QUEEN);
        assertFalse(PokerHandEvaluator.isThreeOfAKind(hand));
    }



    /**
     * FOUR OF A KIND TESTS
     */


    @Test
    public void isFourOfAKind_ShouldSucceed_when_Cards_HEARTS_QUEEN_DIAMIOND_QUEEN_CLUBS_QUEEN_HEART_NINE_DIAMOND_TWO_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(HEARTS_QUEEN, DIAMONDS_QUEEN, CLUBS_QUEEN, SPADES_QUEEN, DIAMONDS_TWO);
        assertTrue(PokerHandEvaluator.isFourOfAKind(hand));
    }

    @Test
    public void isFourOfAKind_ShouldFail_when_Cards_HEARTS_QUEEN_DIAMIOND_QUEEN_CLUBS_QUEEN_HEART_NINE_DIAMOND_TWO_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(HEARTS_QUEEN, DIAMONDS_QUEEN, CLUBS_QUEEN, HEARTS_NINE, DIAMONDS_TWO);
        assertFalse(PokerHandEvaluator.isFourOfAKind(hand));
    }


    /**
     * FLUSH TESTS
     */

    @Test
    public void isFlush_ShouldSucceed_when_Cards_HEARTS_QUEEN_HEARTS_JACK_HEARTS_NINE_HEARTS_SIX_HEARTS_TWO_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(HEARTS_QUEEN, HEARTS_JACK, HEARTS_NINE, HEARTS_SIX, HEARTS_TWO);
        assertTrue(PokerHandEvaluator.isFlush(hand));
    }

    @Test
    public void isFlush_ShouldSucceed_when_Cards_DIAMONDS_JACK_HEARTS_JACK_HEARTS_NINE_HEARTS_SIX_HEARTS_TWO_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(DIAMONDS_JACK, HEARTS_JACK, HEARTS_NINE, HEARTS_SIX, HEARTS_TWO);
        assertFalse(PokerHandEvaluator.isFlush(hand));
    }

    /**
     * FULL HOUSE TESTS
     */

    @Test
    public void isFullHouse_ShouldSucceed_when_Cards_HEARTS_QUEEN_HEARTS_TEN_DIAMONDS_TEN_DIAMONDS_QUEEN_CLUBS_QUEEN_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(HEARTS_QUEEN, HEARTS_TEN, DIAMONDS_TEN, DIAMONDS_QUEEN, CLUBS_QUEEN);
        assertTrue(PokerHandEvaluator.isFullHouse(hand));
    }

    @Test
    public void isFullHouse_ShouldFail_when_Cards_DIAMONDS_JACK_HEARTS_JACK_HEARTS_NINE_DIAMONDS_NINE_HEARTS_TWO_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(DIAMONDS_JACK, HEARTS_JACK, HEARTS_NINE, DIAMONDS_NINE, HEARTS_TWO);
        assertFalse(PokerHandEvaluator.isFullHouse(hand));
    }


    /**
     * STRAIGHT TESTS
     */

    @Test
    public void isStraight_ShouldSucceed_when_Cards_HEARTS_QUEEN_HEARTS_JACK_CLUBS_NINE_CLUBS_NINE_HEARTS_TEN_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(HEARTS_QUEEN, HEARTS_JACK, CLUBS_NINE, CLUBS_EIGHT, HEARTS_TEN);
        assertTrue(PokerHandEvaluator.isStraight(hand));
    }


    @Test
    public void isStraight_ShouldFail_when_Cards_HEARTS_QUEEN_HEARTS_JACK_CLUBS_NINE_CLUBS_NINE_HEARTS_TEN_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(HEARTS_QUEEN, HEARTS_JACK, CLUBS_KING, CLUBS_ACE, HEARTS_TWO);
        assertFalse(PokerHandEvaluator.isStraight(hand));
    }

    @Test
    public void isStraight_ShouldFail_when_Cards_HEARTS_QUEEN_DIAMONDS_QUEEN_CLUBS_NINE_CLUBS_EIGHT_HEARTS_TEN_PassedIn() throws Exception {
        PokerHand hand = new PokerHand(HEARTS_QUEEN, DIAMONDS_QUEEN, CLUBS_NINE, CLUBS_EIGHT, HEARTS_TEN);
        assertFalse(PokerHandEvaluator.isStraight(hand));
    }
}