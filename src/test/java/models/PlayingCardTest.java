package models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class PlayingCardTest {
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void random() throws Exception {

        final int NR_OF_PICKS = 520;
        Map<String, Integer> randomCards =  new HashMap<String, Integer>();

        for(int i=0; i< NR_OF_PICKS; i++){
            PlayingCard card = PlayingCard.random();

            Integer alreadyExist = randomCards.get(card.toString());
            if(alreadyExist == null){
                randomCards.put(card.toString(), 1);
            }else{
                randomCards.put(card.toString(), ++alreadyExist);
            }
        }

        Collection<Integer> histogram = randomCards.values();


        System.out.printf("size: %d content: %s",histogram.size(), histogram.toString());

        final int EXPECTED_NR_OF_CARDS = 52;
        final int actual = histogram.size();


        final Optional<Integer> max = histogram
                            .stream()
                            .max(Comparator.naturalOrder());

        final Optional<Integer> min = histogram
                            .stream()
                            .min(Comparator.naturalOrder());



        final int MAX_DELTA = (int) (NR_OF_PICKS * 0.03);

        Optional<Boolean> isRandomEnough = min.flatMap(mi -> max.flatMap(ma -> Optional.of(Math.abs(mi-ma) < MAX_DELTA)));

        assert(isRandomEnough.orElse(false) & histogram.size() == EXPECTED_NR_OF_CARDS);

    }

    @Test
    public void to_string() throws Exception {
        final String expected = "CLUBS_ACE";
        final String actual = new PlayingCard(CARDCOLOR.CLUBS, CARDVALUE.ACE).toString();

        assertEquals(expected, actual);

    }

}